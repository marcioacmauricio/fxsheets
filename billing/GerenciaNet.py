# -*- coding: utf-8 -*-
from utilities import keepdict, pretty
from gerencianet import Gerencianet
import pdfkit
from datetime import datetime, timedelta, date
from brazilnum.util import clean_id
import os
import sys
class GerenciaNet:
	"""docstring for GerenciaNet"""
	isSandbox = None
	def __init__(self,ClientId, ClientSecret, Sandbox, Detail):
		self.isSandbox = Sandbox
		self.Detail = Detail
		Options = {
			'client_id': ClientId,
			'client_secret': ClientSecret,
			'sandbox': Sandbox
		}
		self.GN = Gerencianet(Options)
	def CancelCharge(self,idTicket):
		Return = {
			'Status': False,
			'ErrorMessage': [],
			'StatusPayment': ''
		}
		params = {
			'id': int(idTicket)
		}
		Response = self.GN.cancel_charge(params=params)
		if Response.get('code') != 200:
			Return['ErrorMessage'].append('Falha ao cancelar.')
			return Return
		else:
			Return['Status'] = True
			return Return
	def CheckStatus(self,idTicket):
		Return = {
			'Status': False,
			'ErrorMessage': [],
			'StatusPayment': ''
		}
		params = {
			'id': int(idTicket)
		}
		Response =  self.GN.detail_charge(params=params)
		if Response.get('code') != 200:
			Return['ErrorMessage'].append('Falha ao verificar')
			return Return
		Return['Status'] = True
		Return['StatusPayment'] = Response.get('data').get('status')
		return Return	
	def IssueTicket(self, Service, Taker, Detail):
		Return = {
			'Status':True,
			'ErrorMessage':[],
			'Data': {}
		}
		Item = {
			"amount": 1,
			"value": int(Service.get('ValorServicos').replace('.',''))
		}
		
		try:
			NfseNumber = int(Service.get('NfseNumber'))
			isNF = True
		except Exception as e:
			isNF = False
		if isNF:
			Item['name'] = "Total de serviços referente à Nota Fiscal N° " + str(NfseNumber)
		else:
			Item['name'] = "Total de serviços referente à Fatura N° " + str(NfseNumber)

		body = {
			'items': [Item]
		}
		NewTicket = self.GN.create_charge(body=body)
		params = {}
		if NewTicket.get('code') != 200:
			Return['Status'] = False
			if 'message' in NewTicket.get('error_description'):
				Return['ErrorMessage'].append(str(NewTicket.get('error_description').get('message')).encode('utf-8'))
			else:
				Return['ErrorMessage'].append(str(NewTicket.get('error_description')))
			return Return
		else:
			params = {
				'id': NewTicket.get('data').get('charge_id')
			}
		body = {
			'payment': {
				'banking_billet': {
					'expire_at': Service.get('DataVencimento'),
					'message': Detail,
					'customer': {
						'name': Taker.get('NomeContato'),
						'email': Taker.get('EmailEnvio')
					}
				}
			}
		}
		if Taker.get('SujeitoAtivo') == 'PJ':
			body['payment']['banking_billet']['customer']['juridical_person'] = {
				'corporate_name': Taker.get('RazaoSocial'),
				'cnpj': clean_id(Taker.get('Cnpj'))
			}
			if (clean_id(Taker.get('Cpf'))):
				body['payment']['banking_billet']['customer']['cpf'] = clean_id(Taker.get('Cpf'))
		else:
			body['payment']['banking_billet']['customer']['cpf'] = clean_id(Taker.get('Cpf'))
		if bool(Taker.get('Telefone')):
			body['payment']['banking_billet']['customer']['phone_number'] = Taker.get('Telefone')
		Ticket = self.GN.pay_charge(params=params, body=body)
		if Ticket.get('code') != 200:
			Return['Status'] = False
			Return['ErrorMessage'].append(Ticket.get('error_description').get('message').encode('utf-8'))
			Return['ErrorMessage'].append(str(Ticket.get('error_description').get('property')).encode('utf-8'))
			return Return
		else:
			FileName = str(Ticket.get('data').get('charge_id')) + '.pdf'
			PathAttach = os.path.abspath(os.path.join(os.path.dirname(__file__), "..")) + '/' + 'billing/tickets/' + FileName
			pdfkit.from_url(Ticket.get('data').get('link'), PathAttach)
			Return['Data'] = {
				'NroBoleto': Ticket.get('data').get('charge_id'),
				'PathAttach': PathAttach,
				'FileName': FileName,
				'Status': Ticket.get('data').get('status'),
				'Total': Ticket.get('data').get('total')
			}
		return Return