from requests import Session
from zeep import Client
from zeep.transports import Transport

class ZeepClient(Client):
	def __init__(self, UrlEnv, PathCrt, PathKey ):
		session = Session()
		session.cert = (PathCrt, PathKey)
		session.verify = True        
		super(ZeepClient, self).__init__(UrlEnv, transport=Transport(session=session))
