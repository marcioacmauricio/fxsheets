# -*- coding: utf-8 -*-
from clean_str import clean_str
from deunicodify_hook import deunicodify_hook
import xmltodict
import os
import sys
import json
from collections import OrderedDict
class keepdict(OrderedDict):
	Type = None
	FilenName = None
	Directories = []
	Path = ''
	def __init__(self,Path,Default = None):
		super(keepdict, self).__init__()
		self.Directories = []		
		Parts = Path.split('.')
		if len(Parts) < 2:
			Msg = "Incomplete name keepdict('" + str(ObjectName) + "') of object !"
			raise ValueError(Msg)
		self.Type = Parts[len(Parts) - 1]
		del Parts[len(Parts) - 1]
		NewPath = '.'.join(Parts)
		NewParts = NewPath.split('/')
		self.FilenName = clean_str(NewParts[len(NewParts) -1 ]) + '.' + self.Type
		del NewParts[len(NewParts) - 1]
		for i in range(len(NewParts)):
			self.Directories.append(clean_str(NewParts[i]))
		Path = self.getPath()
		if not os.path.isfile(Path):
			if bool(Default):
				DefaultData = keepdict(Default)
				self.update(DefaultData)
			self.save()
		else:
			if self.Type == 'xml':
				XmlData = open(Path).read().encode('utf-8')
				if bool(XmlData):
					Data = xmltodict.parse(XmlData)
					self.update(Data)
			elif self.Type == 'json':
				JsonData = open(Path).read().encode('utf-8')
				if bool(JsonData):
					Data = json.loads(JsonData.encode('utf-8'),object_pairs_hook=deunicodify_hook)
					self.update(Data)
	def getPath(self):
		return self.getDir(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")) + "/" + '/'.join(self.Directories)) + '/' + self.FilenName
	def jsonify(self, pretty = True):
		if pretty:
			return json.dumps(self,indent=4,sort_keys=True)	
		else:
			return json.dumps(self,sort_keys=True)
	def save(self):
		if self.Type == 'xml':
			if len(self):
				Content = xmltodict.unparse(self, pretty=True)
			else:
				Content = ''
		elif self.Type == 'json':
			Content = self.jsonify()
		FL = open( self.getPath(), "w")
		FL.write( Content.encode('utf-8') )
		FL.close()
	def xmlify(self, pretty = True):
		return xmltodict.unparse(self, pretty=pretty)
	def getDir(self,PathDir):
		if not os.path.exists(PathDir):
			os.makedirs( PathDir, 0o777)
		return PathDir
	def move(self,Key,Value):
		CurPath = self.getPath()
		self.Directories[Key] = clean_str(Value)
		NewPath = self.getPath()
		os.rename(CurPath,NewPath)
	def empty(self):
		if self.Type == 'xml':
			Content = ''
		elif self.Type == 'json':
			Content = '{}'
		FL = open(self.getPath(), "w")
		FL.write( Content.encode('utf-8') )
		FL.close()		

		