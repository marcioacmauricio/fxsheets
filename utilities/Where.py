# -*- coding: utf-8 -*-
def Where(Values, Field, Value):
	Return = {}
	if not bool(Values) or not bool(Field):
		return Return
	for Key, Val in Values.items():
		ValueField = Val.get(Field)
		if ValueField == Value:
			Return.update({Key:Val})
	return Return
def noWhere(Values, Field, Value):
	Return = {}
	if not bool(Values) or not bool(Field):
		return Return
	for Key, Val in Values.items():
		ValueField = Val.get(Field)
		if ValueField != Value:
			Return.update({Key:Val})
	return Return