from oauth2client import tools
try:
	import argparse
	parser = argparse.ArgumentParser(parents=[tools.argparser])
	parser.add_argument("-c", "--configure", help="set initial settings",action="store_true")
	parser.add_argument("-b", "--billing", help="issue accumulated charges",action="store_true")
	parser.add_argument("-n", "--nfse", help="bill accumulated charges",action="store_true")
	parser.add_argument("-t", "--ticket", help="issue accumulated ticket",action="store_true")
	parser.add_argument("-m", "--mailer", help="send accumulated emails",action="store_true")
	parser.add_argument("-M", "--mailtest", help="Test email message",action="store_true")
	parser.add_argument("-B", "--confirm-billing", help="confirm accumulated receipt",action="store_true")
	flags = parser.parse_args()
except ImportError:
	flags = None