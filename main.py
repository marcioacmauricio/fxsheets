#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import sys
reload(sys)  
sys.setdefaultencoding('utf8')
from nfse import Nfse
from utilities import pretty
from pySheets import pySheets
# from utilities import Signing
# Xml = Signing('nfse/rps/1.xml')
# Xml.Sign()
# rps = keepdict('examples/exemploXMLcomAssinatura.xml')
# rps.save()
# print(rps)
UrlEnv = 'https://wsnfse.vitoria.es.gov.br/homologacao/NotaFiscalService.asmx?WSDL'
PathCrt = 'certs/crt.pem'
PathKey = 'certs/key.pem'

Prestador = {
	"Serie": "NFE",
	"Tipo": "1",
	"IssRetido":"2",
	"ItemListaServico":"1.03",
	"CodigoCnae": "6311900",
	"CodigoMunicipio": "3205309",
	"ExigibilidadeISS": "1",
	"Cnpj":"13419453000107",
	"InscricaoMunicipal":"1201640",
	"OptanteSimplesNacional": "1",
	"IncentivoFiscal": "2"
}

Servico = {
	"DataEmissao":"2017-05-30",
	"Competencia":"2017-05-30",
	"ValorServicos":"200",
	"Discriminacao": "Descrição do Novo RPS"
}
NF = Nfse(UrlEnv,PathCrt,PathKey, Prestador)

# Tomador = {
# 	"SujeitoAtivo": "PJ",
# 	"Cnpj":"08832573000193",
# 	"RazaoSocial": "M A C MAURICIO INFORMATICA ERELI - ME",
# 	"Endereco": "AV MARTIN AFONSO DE SOUA",
# 	"Numero":"216",
# 	"Bairro": "INTERLAGOS",
# 	"CodigoMunicipio":"3205309",
# 	"Uf":"ES"
# }
# pretty(NF.GerarNfse(Tomador, Servico))



# Tomador = {
# 	"SujeitoAtivo": "PF",
# 	"Cpf":"07810013742",
# 	"Nome": "Marcio Antonio Caldeira Mauricio",
# 	"Endereco": "AV MARTIN AFONSO DE SOUA",
# 	"Numero":"216",
# 	"Bairro": "INTERLAGOS",
# 	"CodigoMunicipio":"3205309",
# 	"Uf":"ES"
# }
# pretty(NF.GerarNfse(Tomador, Servico))