# -*- coding: utf-8 -*-
import sys
import os
sys.path.insert(0,os.path.abspath(os.path.join(os.path.dirname(__file__), ".")))
from utilities import keepdict, pretty, GroupBy, flags, Where, noWhere
from sheets import Sheets
from nfse import Nfse
from datetime import datetime, timedelta, date
import re
import jinja2
from brazilnum.cnpj import validate_cnpj, format_cnpj
from brazilnum.cpf import validate_cpf
from brazilnum.muni import validate_muni
from brazilnum.util import clean_id
from validate_email import validate_email
from billing import GerenciaNet
from mailer import Mailer

class pySheets:
	Sheets = None
	NextFatura = None
	AtualizarConfiguracoes = None
	ActivateAgent = None
	TestEnvironment = None
	TicketClientId = None
	TicketClientSecret = None
	UrlEnv = None
	PathCrt = 'certs/crt.pem'
	PathKey = 'certs/key.pem'
	Provider = None
	NF = None
	GN = None
	def __init__(self):
		if flags.configure:
			if flags.configure:
				self.setParams()
		self.setState()
		ConfigSheet = keepdict('config/Sheet.json')
		self.TestEnvironment = ConfigSheet.get('TEST_ENVIRONMENT')
		Config = keepdict('config/Config.json')		
		ConfigTicket = Config.get('Ticket')
		ConfigNfse = Config.get('Nfse')
		if self.TestEnvironment:
			self.UrlEnv = ConfigNfse.get('UrlWsHomologacao')
			self.TicketClientId = ConfigTicket.get('SandboxId')
			self.TicketClientSecret = ConfigTicket.get('SandboxSecret')
		else:
			self.UrlEnv = ConfigNfse.get('UrlWsProducao')
			self.TicketClientId = ConfigTicket.get('ProducaoId')
			self.TicketClientSecret = ConfigTicket.get('ProducaoSecret')
		self.TicketDetail = ConfigTicket.get('DetalhementoBoleto')
		self.Provider = self.getProvider()
		self.NF = Nfse(self.UrlEnv,self.PathCrt,self.PathKey, self.Provider.get('Data'))
		self.GN = GerenciaNet(self.TicketClientId, self.TicketClientSecret, self.TestEnvironment, self.TicketDetail)
		if self.ActivateAgent:
			self.Sheets.updateCell('Status','DataStatus',8,datetime.today().strftime("%d/%m/%Y %H:%M:%S"))
			self.Sheets.updateCell('Status','DataStatus',9,'')
			if flags.configure or self.AtualizarConfiguracoes:
				print('configure')
				self.Configure()
				self.Sheets.commit()
				self.setState()
			if flags.billing:
				print('billing')
				self.Billing()
				self.Sheets.commit()
			if flags.nfse:
				print('nfe')
				self.NF.startZeep()
				self.IssueInvoice()
				self.Sheets.commit()
			if flags.ticket:
				print('IssueTicket')				
				self.IssueTicket()
				self.Sheets.commit()
			if flags.mailer:
				print('Notify')
				self.Notify()				
				self.Sheets.commit()
			if flags.mailtest:
				print('mailtest')
				self.Testmail()				
				self.Sheets.commit()				
			if flags.confirm_billing:
				print('confirm_billing')
				self.ConfirmBilling()
				self.Sheets.commit()
			self.Cancel()
			self.Sheets.updateCell('Status','DataStatus',9,datetime.today().strftime("%d/%m/%Y %H:%M:%S"))
			self.Sheets.commit()
			self.setState()
	def Cancel(self):
		toCancel = Where(self.Sheets.get('Registros').get('values'),'Status','Cancelar')
		byIdBoleto = GroupBy(toCancel,'IdBoleto')
		Config = keepdict('config/Config.json')
		UserMail = keepdict('config/Mailer.json')
		MailerConfig = Config.get('Mailer')
		SendMail = Mailer(MailerConfig.get('SmtpServer'), MailerConfig.get('Port'), UserMail.get('Email'), UserMail.get('Password'), MailerConfig.get('FromName'), MailerConfig.get('FromEmail'), self.toState(MailerConfig.get('MensagemHtml')))
		for IdBoleto, Records in byIdBoleto.items():
			idFirstRecord = list(Records.keys())[0]
			FirstRecord = Records.get(idFirstRecord)
			InvoiceId = FirstRecord.get('FaturaNumero')
			if len(Records) == len(Where(self.Sheets.get('Registros').get('values'),'IdBoleto',IdBoleto)):
				CancelCharge = self.GN.CancelCharge(IdBoleto)
				if CancelCharge.get('Status'):
					Message = self.getCancelMessage(MailerConfig, InvoiceId)
					SendMail.Send(**Message)
					for Key, Record in Records.items():
						self.Sheets.updateCell('Registros','ErroMensagem', Record.get('Id'), '')
						self.Sheets.updateCell('Registros','StatusAgente', Record.get('Id'), 'SucessoCancelar')
						self.Sheets.updateCell('Registros','Status', Record.get('Id'), 'Cancelado')
				else:
					for Key, Record in Records.items():
						self.Sheets.updateCell('Registros','ErroMensagem', Record.get('Id'), '\n'.join(CancelCharge.get('ErrorMessage')))
						self.Sheets.updateCell('Registros','StatusAgente', Record.get('Id'), 'ErroCancelar')	
			else:
				for Key, Record in Records.items():
					self.Sheets.updateCell('Registros','ErroMensagem', Record.get('Id'), 'Totoal de itens para o boleto inconsistente com Status')
					self.Sheets.updateCell('Registros','StatusAgente', Record.get('Id'), 'ErroCancelar')

	def Notify(self):
		RecordsBilled = noWhere(self.Sheets.get('Registros').get('values'),'IdBoleto','')
		RecordsUnbiled = noWhere(RecordsBilled,'Status','Faturado')
		byInvoice = GroupBy(RecordsUnbiled,'FaturaNumero')
		Config = keepdict('config/Config.json')
		UserMail = keepdict('config/Mailer.json')
		MailerConfig = Config.get('Mailer')
		SendMail = Mailer(MailerConfig.get('SmtpServer'), MailerConfig.get('Port'), UserMail.get('Email'), UserMail.get('Password'), MailerConfig.get('FromName'), MailerConfig.get('FromEmail'), self.toState(MailerConfig.get('MensagemHtml')))
		for InvoiceId, Records in byInvoice.items():
			Message = self.getDefaultMessage(MailerConfig, InvoiceId)
			if SendMail.Send(**Message):
				for Key, Record in Records.items():
					self.Sheets.updateCell('Registros','ErroMensagem', Record.get('Id'), '')
					self.Sheets.updateCell('Registros','StatusAgente', Record.get('Id'), 'SucessoNotificar')
					self.Sheets.updateCell('Registros','Status', Record.get('Id'), 'Faturado')
			else:
				for Key, Record in Records.items():
					self.Sheets.updateCell('Registros','ErroMensagem', Record.get('Id'), '\n'.join(SendMail.ErrorMessage))
					self.Sheets.updateCell('Registros','StatusAgente', Record.get('Id'), 'ErroNotificar')
					
	def Testmail(self):
		DataConfig = self.getDataConfig()
		MailerConfig = DataConfig.get('Mailer')
		InvoiceId = MailerConfig.get('FaturaModelo')
		EmailTest = MailerConfig.get('EmailTest')
		if bool(InvoiceId) and bool(EmailTest):
			UserMail = keepdict('config/Mailer.json')
			Message = self.getDefaultMessage(MailerConfig, InvoiceId)
			Message['ToEmail'] = MailerConfig.get('EmailTest')
			SendMail = Mailer(MailerConfig.get('SmtpServer'), MailerConfig.get('Port'), UserMail.get('Email'), UserMail.get('Password'), MailerConfig.get('FromName'), MailerConfig.get('FromEmail'), self.toState(MailerConfig.get('MensagemHtml')))
			SendMail.Send(**Message)
			Message = self.getCancelMessage(MailerConfig, InvoiceId)
			Message['ToEmail'] = MailerConfig.get('EmailTest')
			SendMail = Mailer(MailerConfig.get('SmtpServer'), MailerConfig.get('Port'), UserMail.get('Email'), UserMail.get('Password'), MailerConfig.get('FromName'), MailerConfig.get('FromEmail'), self.toState(MailerConfig.get('MensagemHtml')))
			SendMail.Send(**Message)
	def getCancelMessage(self, MailerConfig, InvoiceId):
		Return = {}
		Invoice = keepdict('nfse/data/invoices/' + str(InvoiceId) + '.xml')
		InvoiceParamns = Invoice.get('Invoice').get('Invoice')
		Provider = Invoice.get('Invoice').get('Provider')
		Taker = Invoice.get('Invoice').get('Taker')
		Service = Invoice.get('Invoice').get('Service')
		Ticket = Invoice.get('Invoice').get('Ticket')
		Return['ToName'] = Taker.get('NomeContato')
		Return['ToEmail'] = Taker.get('EmailEnvio')
		Return['EmailCc'] = Taker.get('EmailCc')
		Return['Subject'] = self.Render(MailerConfig.get('CancelamentoSubject'),InvoiceParamns, Provider, Taker, Service, Ticket)
		Return['Content'] = self.Render(MailerConfig.get('CancelamentoBody'),InvoiceParamns, Provider, Taker, Service, Ticket)
		Return['Content'] += self.Render(MailerConfig.get('Disclaimer'),InvoiceParamns, Provider, Taker, Service, Ticket)
		return Return		
	def getDefaultMessage(self, MailerConfig, InvoiceId):
		Return = {}
		Invoice = keepdict('nfse/data/invoices/' + str(InvoiceId) + '.xml')
		InvoiceParamns = Invoice.get('Invoice').get('Invoice')
		Provider = Invoice.get('Invoice').get('Provider')
		Taker = Invoice.get('Invoice').get('Taker')
		Service = Invoice.get('Invoice').get('Service')
		Ticket = Invoice.get('Invoice').get('Ticket')
		Return['ToName'] = Taker.get('NomeContato')
		Return['ToEmail'] = Taker.get('EmailEnvio')
		Return['EmailCc'] = Taker.get('EmailCc')
		Return['Subject'] = self.Render(MailerConfig.get('PadraoSubject'),InvoiceParamns, Provider, Taker, Service, Ticket)
		Return['Content'] = self.Render(MailerConfig.get('PadraoBody'),InvoiceParamns, Provider, Taker, Service, Ticket)
		Return['Content'] += self.Render(MailerConfig.get('Disclaimer'),InvoiceParamns, Provider, Taker, Service, Ticket)
		TicketData = Ticket.get('Data')
		Return['Attachment'] = TicketData.get('PathAttach')
		return Return
	def Render(self, Text, InvoiceParamns, Provider, Taker, Service, Ticket):
		e = jinja2.Environment(extensions=["jinja2.ext.do",])
		try:
			t = e.from_string(Text.decode("utf8"))
			Return = t.render(Fatura = InvoiceParamns, Prestador = Provider, Tomador = Taker, Servico = Service, Boleto = Ticket, bool = bool, pretty = pretty)
		except Exception as e:
			Return = str(e)
		return Return			
	def ConfirmBilling(self):
		RecordsWaiting = Where(self.Sheets.get('Registros').get('values'),'StatusBoleto','waiting')
		byIdBoleto = GroupBy(RecordsWaiting,'IdBoleto')
		for idTicket, Records in byIdBoleto.items():
			chkTicket = self.GN.CheckStatus(idTicket)
			if chkTicket.get('Status'):
				Today = datetime.today()
				idFirstRecord = list(Records.keys())[0]
				FirstRecord = Records.get(idFirstRecord)
				Date = FirstRecord.get('DataVencimento')
				DataVencimento = datetime.strptime(Date,'%d/%m/%Y')
				NewStatus = FirstRecord.get('Status')
				if chkTicket.get('StatusPayment') == 'paid':
					NewStatus = 'Pago'
				elif chkTicket.get('StatusPayment') == 'canceled':
					NewStatus = 'Cancelado'
				elif DataVencimento < Today: 
					NewStatus = 'Vencido'
				for Key, Record in Records.items():
					self.Sheets.updateCell('Registros','ErroMensagem', Record.get('Id'), '')
					self.Sheets.updateCell('Registros','StatusAgente', Record.get('Id'), 'SucessoVerificarPgt')
					self.Sheets.updateCell('Registros','StatusBoleto', Record.get('Id'), chkTicket.get('StatusPayment'))
					self.Sheets.updateCell('Registros','DataStatusPgt', Record.get('Id'), Today.strftime("%d/%m/%Y %H:%M:%S"))
					self.Sheets.updateCell('Registros','Status', Record.get('Id'), NewStatus)
			else:
				for Key, Record in Records.items():
					self.Sheets.updateCell('Registros','ErroMensagem', Record.get('Id'), '\n'.join(chkTicket.get('ErrorMessage')))
					self.Sheets.updateCell('Registros','StatusAgente', Record.get('Id'), 'ErroVerificarPgt')
	def IssueTicket(self):
		Records = noWhere(self.Sheets.get('Registros').get('values'),'NumeroNF','')
		RecsTicketAll = Where(Records,'IdBoleto','')
		byInvoice = GroupBy(RecsTicketAll,'FaturaNumero')
		for InvoiceId, Items in byInvoice.items():
			Invoice = keepdict('nfse/data/invoices/' + str(InvoiceId) + '.xml')
			Service = Invoice.get('Invoice').get('Service')
			NfseNumber = Invoice.get('Invoice').get('Invoice').get('NfseNumber')
			Service['NfseNumber'] = NfseNumber
			Ticket = self.GN.IssueTicket(Service, Invoice.get('Invoice').get('Taker'), self.TicketDetail)
			if Ticket.get('Status'):
				DataTicket = Ticket.get('Data')
				for Key, ItemService in Items.items():
					self.Sheets.updateCell('Registros','ErroMensagem', ItemService.get('Id'), '')
					self.Sheets.updateCell('Registros','StatusAgente', ItemService.get('Id'), 'SucessoBoleto')
					self.Sheets.updateCell('Registros','IdBoleto', ItemService.get('Id'), DataTicket.get('NroBoleto'))
					self.Sheets.updateCell('Registros','StatusBoleto', ItemService.get('Id'), DataTicket.get('Status'))				
					self.Sheets.updateCell('Registros','DataEmissao', ItemService.get('Id'), datetime.today().strftime("%d/%m/%Y %H:%M:%S"))
				Invoice['Invoice']['Ticket'] = Ticket
				Invoice['Invoice']['Invoice']['TicketId'] = DataTicket.get('NroBoleto')
				Invoice.save()
			else:
				for Key, ItemService in Items.items():
					self.Sheets.updateCell('Registros','ErroMensagem', ItemService.get('Id'), '\n'.join(Ticket.get('ErrorMessage')))
					self.Sheets.updateCell('Registros','StatusAgente', ItemService.get('Id'), 'ErroBoleto')
					self.Sheets.updateCell('Registros','IdBoleto', ItemService.get('Id'), '')
					self.Sheets.updateCell('Registros','StatusBoleto', ItemService.get('Id'), '')				
					self.Sheets.updateCell('Registros','DataEmissao', ItemService.get('Id'), '')

	def IssueInvoice(self):
		RecsInvoiceAll = noWhere(self.Sheets.get('Registros').get('values'),'FaturaNumero','')
		RecsInvoice = Where(RecsInvoiceAll,'NumeroNF','')	
		byInvoice = GroupBy(RecsInvoice,'FaturaNumero')
		for InvoiceId, Recs in byInvoice.items():
			checkInvoiceData = self.checkRecsInvoice(Recs)
			if checkInvoiceData.get('Status'):
				checkInvoice = checkInvoiceData.get('Data')
				if checkInvoice.get('SuprimirNfse'):
					for Key, Service in Recs.items():
						self.Sheets.updateCell('Registros','ErroMensagem', Service.get('Id'), '')
						self.Sheets.updateCell('Registros','StatusAgente', Service.get('Id'), 'SucessoNfse')
						self.Sheets.updateCell('Registros','NumeroNF', Service.get('Id'), 'F-' + str(InvoiceId))
						self.Sheets.updateCell('Registros','ChaveNF', Service.get('Id'), 'F-' + str(InvoiceId))
				else:
					if self.NF.Issue(InvoiceId):
						Invoice = keepdict('nfse/data/invoices/' + str(InvoiceId) + '.xml')
						for Key, Service in Recs.items():
							self.Sheets.updateCell('Registros','ErroMensagem', Service.get('Id'), '')
							self.Sheets.updateCell('Registros','StatusAgente', Service.get('Id'), 'SucessoNfse')
							self.Sheets.updateCell('Registros','NumeroNF', Service.get('Id'), Invoice.get('Invoice').get('Invoice').get('NfseNumber'))
							self.Sheets.updateCell('Registros','ChaveNF', Service.get('Id'), Invoice.get('Invoice').get('Invoice').get('VerificationCode'))
					else:
						for Key, Service in Recs.items():
							self.Sheets.updateCell('Registros','ErroMensagem', Service.get('Id'), '\n'.join(self.NF.ErrorMessage))
							self.Sheets.updateCell('Registros','StatusAgente', Service.get('Id'), 'ErroNfse')
			else:
				for Key, Service in Recs.items():
					self.Sheets.updateCell('Registros','ErroMensagem', Service.get('Id'), '\n'.join(checkInvoiceData.get('Message')))
					self.Sheets.updateCell('Registros','StatusAgente', Service.get('Id'), 'ErroNfse')
	def checkRecsInvoice(self, Records):
		Return = {}
		Return['Status'] = True
		Return['Message'] = []
		Return['Data'] = {}
		idFirstRecord = list(Records.keys())[0]
		FirstRecord = Records.get(idFirstRecord)
		TakerName = FirstRecord.get('TomadorNome')
		TakerData = self.CheckTaker(TakerName)
		Taker = TakerData.get('Data')
		GeraNfse = self.toState(Taker.get('SuprimirNfse'))
		for RecId, Record in Records.items():
			if FirstRecord.get('SuprimirNfse') != Record.get('SuprimirNfse'):
				Return['Status'] = False
				Return['Message'].append("Inconsistencia em coluna 'GeraNfse' na guia 'Registros'")
		if self.toState(FirstRecord.get('SuprimirNfse')):
			Return['Data']['SuprimirNfse'] = True
		elif GeraNfse:
			Return['Data']['SuprimirNfse'] = True
		else:
			Return['Data']['SuprimirNfse'] = False
		return Return

	def Billing(self):
		ToBill = self.WithinPeriod()
		if bool(ToBill):
			byTaker = GroupBy(ToBill,'TomadorNome')
			for TakerName, byTakerItems in byTaker.items():
				Taker = self.CheckTaker(TakerName)
				byDate = GroupBy(byTakerItems,'DataVencimento')
				for Date, byDateItems in byDate.items():
					Service = self.getService(byDateItems, TakerName, Date)
					self.BillItem(Taker, Service)
	def BillItem(self, Taker, Service):
		Status = True
		Messages = []
		if bool(Taker.get('Status')):
			self.Sheets.updateCell('Clientes','ErroMensagem',Taker.get('Data').get('Id'),'')
			self.Sheets.updateCell('Clientes','StatusAgente',Taker.get('Data').get('Id'),'')
		else:
			Status = False
			Message = "\n".join(Taker.get('Message'))
			Messages.append('Cliente(' + Message + ")")
			self.Sheets.updateCell('Clientes','ErroMensagem',Taker.get('Data').get('Id'),Message)
			self.Sheets.updateCell('Clientes','StatusAgente',Taker.get('Data').get('Id'),'Erro')
		if not bool(self.Provider.get('Status')):
			Status = False
			Message = "\n".join(self.Provider.get('Message'))
			Messages.append('Prestador(' + Message + ")")
		if not bool(Service.get('Status')):
			Status = False
			Message = "\n".join(Service.get('Message'))
			Messages.append('Registros(' + Message + ")")
		if Status:
			IdInvoice = self.NF.Bill(Taker.get('Data'), Service.get('Data'))
			for Key, Service in Service.get('ServiceItems').items():
				self.Sheets.updateCell('Registros','ErroMensagem', Service.get('Id'), '')
				self.Sheets.updateCell('Registros','StatusAgente', Service.get('Id'), 'Faturado')
				self.Sheets.updateCell('Registros','FaturaNumero', Service.get('Id'), IdInvoice)
		else:
			for Key, Service in Service.get('ServiceItems').items():
				self.Sheets.updateCell('Registros','ErroMensagem', Service.get('Id'), '\n'.join(Messages))
				self.Sheets.updateCell('Registros','StatusAgente', Service.get('Id'), 'ErroAoFaturar')
	def getService(self, ServiceItems, TakerName, Date):
		Return = {}
		Return['Status'] = True
		Return['Message'] = []
		Return['Data'] = {}
		Return['ServiceItems'] = ServiceItems
		Prestador = {}
		Discriminacao = []
		ValorServicos = 0.0
		RecsTaker = Where(self.Sheets.get('Registros').get('values'),'TomadorNome',TakerName)
		RecsDate = Where(RecsTaker,'DataVencimento',Date)
		if len(ServiceItems) != len(RecsDate):
			Return['ServiceItems'] = RecsDate
			Return['Status'] = False
			Return['Message'].append('Total de registros incoerente para Status/Cliente/Data')
		for Key, Record in ServiceItems.items():
			if bool(Record.get('DataStatusPgt')):
				Return['Status'] = False
				Return['Message'].append("Campo 'DataStatusPgt' preenchido para nova fatura!")
			if bool(Record.get('NumeroNF')):
				Return['Status'] = False
				Return['Message'].append("Campo 'NumeroNF' preenchido para nova fatura!")
			if bool(Record.get('ChaveNF')):
				Return['Status'] = False
				Return['Message'].append("Campo 'ChaveNF' preenchido para nova fatura!")
			if bool(Record.get('IdBoleto')):
				Return['Status'] = False
				Return['Message'].append("Campo 'IdBoleto' preenchido para nova fatura!")
			if bool(Record.get('StatusBoleto')):
				Return['Status'] = False
				Return['Message'].append("Campo 'StatusBoleto' preenchido para nova fatura!")
			if bool(Record.get('DataEmissao')):
				Return['Status'] = False
				Return['Message'].append("Campo 'DataEmissao' preenchido para nova fatura!")
			try:
				ValorServicos += float(Record.get('ValorServicos').replace(',','.'))
			except Exception as e:
				Return['Status'] = False
				Return['Message'].append("Campo 'ValorServicos' inválido!")
			if bool(Record.get('DescricaoEstruturada')):
				Discriminacao.append(Record.get('DescricaoEstruturada'))
			else:
				Return['Status'] = False
				Return['Message'].append("Campo 'DescricaoEstruturada' inválido!")
		FieldsService = keepdict('nfse/data/Fields.json').get('Service')
		Service = {}
		Service['DataEmissao'] = datetime.today().strftime("%Y-%m-%d")
		Service['Competencia'] = datetime.today().strftime("%Y-%m-%d")
		Service['DataVencimento'] = datetime.strptime(Date,"%d/%m/%Y").strftime("%Y-%m-%d")
		Service['ValorServicos'] = "%.2f" % ValorServicos
		Service['Discriminacao'] = "\n".join(Discriminacao)
		for FieldService, Value in FieldsService.items():
			if not bool(Service.get(FieldService)):
				Return['Status'] = False
				Return['Message'].append("Valor inválido para o campo '" + str(FieldService) + "' em Serviço Faturado!")
		Return['Data'] = Service	
		return Return			
	def getProvider(self):
		Return = {}
		Return['Status'] = True
		Return['Message'] = []
		Return['Data'] = {}
		Prestador = {}
		Config = keepdict('config/Config.json')
		Fields = keepdict('nfse/data/Fields.json')
		FieldsProvider = Fields.get('Provider')
		Prestador['Serie'] = Config.get('Nfse').get('Serie')
		Prestador['Tipo'] = Config.get('Nfse').get('Tipo')
		Prestador['IssRetido'] = Config.get('Nfse').get('IssRetido')
		Prestador['ItemListaServico'] = Config.get('Nfse').get('ItemListaServico')
		Prestador['CodigoCnae'] = Config.get('Nfse').get('CodigoCnae')		
		Prestador['ExigibilidadeISS'] = Config.get('Nfse').get('ExigibilidadeISS')
		Prestador['OptanteSimplesNacional'] = Config.get('Nfse').get('OptanteSimplesNacional')
		Prestador['IncentivoFiscal'] = Config.get('Nfse').get('IncentivoFiscal')
		Prestador['InscricaoMunicipal'] =  Config.get('Prestador').get('InscricaoMunicipal')
		if validate_cnpj(Config.get('Prestador').get('Cnpj')):
			Prestador['Cnpj'] = clean_id(Config.get('Prestador').get('Cnpj'))
		else:
			Return['Status'] = False
			Return['Message'].append("'Cnpj' inválido!")
		if validate_muni(Config.get('Nfse').get('CodigoMunicipio')):
			Prestador['CodigoMunicipio'] = Config.get('Nfse').get('CodigoMunicipio')
		else:
			Return['Status'] = False
			Return['Message'].append("'CodigoMunicipio' inválido!")
		for FieldProvider, Value in FieldsProvider.items():
			if not bool(Prestador.get(FieldProvider)):
				Return['Status'] = False
				Return['Message'].append("Valor inválido para o campo '" + str(FieldProvider) + "' em Configurações de Prestador!")
		Return['Data'] = Prestador
		return Return
	def CheckTaker(self,TakerName):
		Return = {}
		Return['Status'] = True
		Return['Message'] = []
		Return['Data'] = {}
		Clientes = self.Sheets.get('Clientes')
		if not bool(Clientes):
			Return['Message'].append("Tomador não localizado!".encode('utf-8'))
			Return['Status'] = False
			return Return
		Cls = Where(Clientes.get('values'),'TomadorNome',TakerName)
		if not bool(Cls):
			Return['Message'].append("Tomador não localizado!".encode('utf-8'))
			Return['Status'] = False
			return Return
		Cliente = Cls.get(list(Cls.keys())[0])
		Return['Data'] = Cliente	
		try:
			Antecedencia = int(Cliente.get('Antecedencia'))
		except Exception as e:
			Antecedencia = 0
		if not bool(Antecedencia):
			Return['Status'] = False
			Return['Message'].append("Erro em 'Antecedencia'")
		fil = re.compile('([0-9]+)')
		if not bool(Cliente.get('Cep')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'Cep'")
		if not bool(Cliente.get('Cidade')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'Cidade'")
		if Cliente.get('SujeitoAtivo') == 'PJ':
			if not validate_cnpj(Cliente.get('Cnpj')):
				Return['Status'] = False
				Return['Message'].append("Erro em 'Cnpj'")
			else:
				Return['Data']['Cnpj'] = clean_id(Cliente.get('Cnpj'))
			if not bool(Cliente.get('RazaoSocial')):
				Return['Status'] = False
				Return['Message'].append("Erro em 'RazaoSocial'")
		elif Cliente.get('SujeitoAtivo') == 'PF':
			if not validate_cpf(Cliente.get('Cpf')):
				Return['Status'] = False
				Return['Message'].append("Erro em 'Cpf'")
			else:
				Return['Data']['Cpf'] = clean_id(Cliente.get('Cpf'))
		else:
			Return['Status'] = False
			Return['Message'].append("Erro em 'SujeitoAtivo'")
		if not validate_email(Cliente.get('EmailEnvio'),check_mx=False):
			Return['Status'] = False
			Return['Message'].append("Erro em 'EmailEnvio'")
		if not bool(Cliente.get('Cidade')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'Cidade'")
		if not validate_muni(Cliente.get('CodigoMunicipio')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'CodigoMunicipio'")
		if not bool(Cliente.get('Endereco')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'Endereco'")
		if not bool(Cliente.get('TomadorNome')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'TomadorNome'")
		if not bool(Cliente.get('Numero')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'Numero'")
		if not bool(Cliente.get('SujeitoAtivo')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'SujeitoAtivo'")
		if not bool(Cliente.get('Uf')):
			Return['Status'] = False
			Return['Message'].append("Erro em 'Uf'")	
		return Return	
	def WithinPeriod(self):
		Registros = Where(self.Sheets.get('Registros').get('values'), 'Status', 'Faturar')
		Unbilled = Where(Registros,'FaturaNumero','')
		Today = datetime.today()
		Return = {}
		for Key, Value in Unbilled.items():
			Antecedencia = 0
			AdvanceDate = timedelta(Antecedencia)
			Advance = timedelta(Antecedencia)
			Status = True			
			try:
				Antecedencia = int(Value.get('Antecedencia'))
			except Exception as e:
				Status = False
				Antecedencia = 0
			if Status:
				Advance = timedelta(Antecedencia)
			try:
				DataVencimento = datetime.strptime(Value.get('DataVencimento'),'%d/%m/%Y')
			except Exception as e:
				DataVencimento = datetime.today()
				Status = False			
			if Status:				
				AdvanceDate = DataVencimento - Advance
				if DataVencimento < Today:
					self.Sheets.updateCell('Registros','ErroMensagem',Value.get('Id'),'Data de vencimento inválida')
					self.Sheets.updateCell('Registros','StatusAgente',Value.get('Id'),'ErroFaturar')
				elif AdvanceDate <= Today:
					Return.update({Key:Value})
					self.Sheets.updateCell('Registros','ErroMensagem',Value.get('Id'),'')
					self.Sheets.updateCell('Registros','StatusAgente',Value.get('Id'),'')
		return Return
	def getDataConfig(self):
		DataConfig = self.Sheets.get('Configuracao')
		if not bool(DataConfig):
			return False
		Values = DataConfig.get('values')
		Keys = []
		for key, value in Values.items():

			Keys.append(int(key))
		Keys.sort()
		SubKey = None
		newData = {}
		for i in range(len(Keys)):
			Value = Values.get(str(Keys[i]))
			newKey = str(Value.get('ConfigNome')).encode('utf-8')
			if newKey[0] == '_':
				SubKey = Value.get('ConfigValor').encode('utf-8')
				newData[SubKey] = {}
			else:
				if newKey in newData[SubKey]:
					if type(newData[SubKey][newKey]) != list:
						newData[SubKey][newKey] = [newData[SubKey][newKey]]

					newData[SubKey][newKey].append(Value.get('ConfigValor').encode('utf-8'))					
				else:
					newData[SubKey][newKey] = Value.get('ConfigValor').encode('utf-8')
		return newData		
	def Configure(self):
		newData = self.getDataConfig()
		Config = keepdict('config/Config.json')
		Config.update(newData)
		Config.save()
		self.Sheets.updateCell('Status','Valor',3,'0')
		self.Sheets.updateCell('Status','DataStatus',3,datetime.today().strftime("%d/%m/%Y %H:%M:%S"))
		self.setState()		
	def setParams(self):
		ConfigSheet = keepdict('config/Sheet.json')
		TEST_ENVIRONMENT = raw_input("Test environment. (enter 1 for yes and 0 for no): ")
		if bool(TEST_ENVIRONMENT):
			ConfigSheet.update({'TEST_ENVIRONMENT':self.toState(TEST_ENVIRONMENT)})	
			ConfigSheet.save()
		SHEET_KEY = raw_input("Paste Spread Sheets Key. (ctrl + shift + v): ")
		if bool(SHEET_KEY):
			ConfigSheet.update({'SHEET_KEY':SHEET_KEY})
			ConfigSheet.save()
		ConfigMailer = keepdict('config/Mailer.json')
		Email = raw_input("E-mail Google Account: ")
		if bool(Email):
			ConfigMailer.update({'Email':Email})		
		Password = raw_input("Password Google Account: ")
		if bool(Password):
			ConfigMailer.update({'Password':Password})
			ConfigMailer.save()
	def setState(self):
		self.Sheets = Sheets()
		if bool(self.Sheets.get('Status')):
			Status = self.Sheets.get('Status').get('values')
			self.ActivateAgent = self.toState(Status.get('2').get('Valor'))
			self.AtualizarConfiguracoes = self.toState(Status.get('3').get('Valor'))
	def toState(self,Value):
		Ret = 0
		try:
			Ret = int(Value)
		except Exception as e:
			Ret = 0
		if bool(Ret):
			return True
		else:
			return False