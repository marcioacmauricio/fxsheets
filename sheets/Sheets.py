from utilities import keepdict, pretty, flags
import string
import json
import httplib2
import os
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import time
class Sheets(keepdict):
	SCOPES = None
	CLIENT_SECRET_FILE = None
	APPLICATION_NAME = None
	SHEET_KEY = None
	SHEET_NAME = None
	discoveryUrl = None
	ColumnIni = None
	ColumnEnd = None
	LineIni = 1
	LineEnd = None
	CellIni = None
	CellEnd = None
	ViewData = {}
	UpdateData = []
	def __init__(self):
		super(Sheets, self).__init__('sheets/Sheets.json')
		self.Config = keepdict('config/Sheet.json')
		self.SCOPES = self.Config['SCOPES']
		self.CLIENT_SECRET_FILE = self.Config['CLIENT_SECRET_FILE']
		self.APPLICATION_NAME = self.Config['APPLICATION_NAME']
		self.discoveryUrl = self.Config['DISCOVERY_URL']
		self.SHEET_KEY = self.Config['SHEET_KEY']
		self.setData()
	def getCredentials(self):
		home_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
		credential_dir = os.path.join(home_dir, 'credentials')
		if not os.path.exists(credential_dir):
			os.makedirs(credential_dir)
		credential_path = os.path.join(credential_dir,'sheets.googleapis.com-python-quickstart.json')
		store = Storage(credential_path)
		credentials = store.get()
		if not credentials or credentials.invalid:
			flow = client.flow_from_clientsecrets(self.CLIENT_SECRET_FILE, self.SCOPES)
			flow.user_agent = self.APPLICATION_NAME
			if flags:
				credentials = tools.run_flow(flow, store, flags)
			else: # Needed only for compatibility with Python 2.6
				credentials = tools.run(flow, store)
		return credentials
	def getService(self):
		credentials = self.getCredentials()
		http = credentials.authorize(httplib2.Http())
		return discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=self.discoveryUrl)	
	def LetterToNumber(self,Str):
		if not bool(Str):
			return 0
		if len(Str) == 1:
			return self.toNumber(Str.upper())
		elif len(Str) == 2:
			First = self.toNumber(Str[0].upper()) + 1
			return (First * len(string.uppercase)) + self.toNumber(Str[1].upper())
	def toNumber(self,Str):
		Letther = string.uppercase
		Index = Letther.index(Str)
		return Index		
	def NumberToLetther(self,Key):
		if int(Key) < len(string.uppercase):
			return self.toLetther(Key)
		else:
			Mod = Key % len(string.uppercase)
			First = self.toLetther((Key - Mod) / len(string.uppercase) - 1)
			return First + self.toLetther(Mod)	
	def toLetther(self,Key):
		Letther = string.uppercase
		return Letther[Key]	
	def getLine(self,Value):
		numColumnIni = self.LetterToNumber(self.ColumnIni)
		numColumnEnd = self.LetterToNumber(self.ColumnEnd)
		Lenght = numColumnEnd - numColumnIni + 1
		Return = {}
		for key in range(Lenght):
			IndexNumber = key + numColumnIni
			IndexLetther = self.NumberToLetther(IndexNumber)			
			if key >= len(Value):
				Return.update({IndexLetther:''})
			else:
				Return.update({IndexLetther:Value[key]})
				
		return Return
	def setCellToUpdate(self,Tab, Letther,LineNumber,Value):
		NewRange = {}
		NewRange['range'] = Tab + '!' + Letther.upper() + str(LineNumber)
		NewRange['values'] = [[Value]]
		self.UpdateData.append(NewRange)
	def updateCell(self,Tab,ColumnName,LineNumber,Value):
		if not bool(self.get(Tab)):
			return False
		Letther = self[Tab]['keys'].get(ColumnName)
		if not bool(Letther):
			Letther = self.NumberToLetther(len(self[Tab]['keys']))
			self.setCellToUpdate(Tab,Letther,1,ColumnName)
		self.setCellToUpdate(Tab,Letther,LineNumber,Value)
	def insertCell(self,Tab,ColumnName,Value):
		if not bool(self.get(Tab)):
			return False
		Letther = self[Tab]['keys'].get(ColumnName)
		if not bool(Letther):
			Letther = self.NumberToLetther(len(self[Tab]['keys']))
			self.setCellToUpdate(Tab,Letther,1,ColumnName)
		LineNumber = self[Tab]['rowCount'] + 2
		self.setCellToUpdate(Tab,Letther,LineNumber,Value)		
	def commit(self):
		if bool(self.UpdateData):
			service = self.getService()
			body = {
				'valueInputOption': 'USER_ENTERED',
				'data': self.UpdateData
			}			
			result = service.spreadsheets().values().batchUpdate(spreadsheetId=self.SHEET_KEY, body=body).execute()
			if bool(result):
				self.UpdateData = []
			self.setData()
	def getTab(self,Values):
		Range = Values.get('range')
		Return = {}
		if not bool(Range):
			return {}
		Return['Tab'] = Range.split('!')[0]
		Vals = Values.get('values')
		Return['rowCount'] = len(Vals) - 1
		Return['values'] = {}
		Return['columnCount'] = len(Vals[0])
		ColumNames = Vals[0]
		for i in range(len(Vals)):
			Val = Vals[i]
			if i == 0:
				Keys = {}
				for j in range(Return['columnCount']):
					Keys[Val[j]] = self.NumberToLetther(j)
				Return['keys'] = Keys
			else:
				NewVal = {}
				NewVal['Id'] = str(i + 1)
				for j in range(Return['columnCount']):
					Column = ColumNames[j]
					if j >= len(Val):
						NewVal[Column] = ''
					else:
						NewVal[Column] = str(Val[j]).encode('utf-8')
				Return['values'][str(i + 1)] = NewVal 
		return Return
	def setData(self):
		self.empty()
		service = self.getService()
		range_names = ['Registros','Clientes','Configuracao','Status']
		result = service.spreadsheets().values().batchGet(spreadsheetId=self.SHEET_KEY, ranges=range_names).execute()
		valueRanges = result.get('valueRanges')
		if bool(valueRanges):
			for i in range(len(valueRanges)):
				Tab = self.getTab(valueRanges[i])
				self.update({Tab['Tab']:Tab})
		self.save()


			



