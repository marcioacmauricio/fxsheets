# -*- coding: utf-8 -*-
from signxml import XMLSigner, XMLVerifier
import xml.etree.ElementTree as ElementTree
from lxml import etree
import os
import sys
# from utilities import ZeepClient, Signing, pretty, keepdict, Assinatura, Signing
from utilities import ZeepClient, keepdict, pretty
from collections import OrderedDict
import xmltodict
from lxml import etree
from signxml import XMLSigner, methods, namespaces
class Nfse:
	Status = True
	UrlEnv = None
	PathCrt = None
	PathKey = None
	Provider = {}
	Status = True
	ErrorMessage = []
	Template = None
	Historic = keepdict('nfse/data/Historic.xml')
	Fields =  keepdict('nfse/data/Fields.json')
	ZeepClient = None
	def __init__(self, UrlEnv, PathCrt, PathKey, Provider):
		self.Provider = Provider
		self.UrlEnv = UrlEnv
		self.PathCrt = PathCrt
		self.PathKey = PathKey
		self.Status = True
		self.ErrorMessage = []	
	def startZeep(self):
		try:
			self.ZeepClient = ZeepClient(self.UrlEnv, self.PathCrt, self.PathKey)
		except Exception as e:
			self.ZeepClient = None
			self.Status = False
			self.ErrorMessage.append('Servidor Indisponível')
		
	def setValues(self, EntityName, Values):
		Fields = self.Fields.get(EntityName)
		for Field, Paths in Fields.items():
			Value = Values.get(Field)
			if bool(Value):
				self.setFields(Paths, Value)
			else:
				self.Status = False
				self.ErrorMessage.append("Field '" + str(Field) + "' of '" + str(EntityName) + "' can not be null")
	def setFields(self,Fields, Value):
		if bool(Fields):
			for i in range(len(Fields)):
				self.setValue(Fields[i], Value)
	def setValue(self, PathField, Value):
		if len(PathField) == 2:
			self.Template[PathField[0]][PathField[1]] = Value
		else:
			DictValue = self.Template[PathField[0]]
			for i in range(1, len(PathField) - 1):
				if i == len(PathField) - 2:
					DictValue = DictValue[PathField[i]]
					DictValue[PathField[i + 1]] = Value
				else:
					DictValue = DictValue[PathField[i]]
	def setIdRPS(self):
		Invoice = self.Historic.get('Invoice')
		if not bool(Invoice):
			self.Historic['Invoice'] = 1
			self.Historic.save()
			Invoice = 1
		self.Template['GerarNfseEnvio']['Rps']['InfDeclaracaoPrestacaoServico']['@Id'] = 'IDPS-' + str(Invoice)
		self.Template['GerarNfseEnvio']['Rps']['InfDeclaracaoPrestacaoServico']['Rps']['@Id'] = 'RPS-' + str(Invoice)
		self.Template['GerarNfseEnvio']['Rps']['InfDeclaracaoPrestacaoServico']['Rps']['IdentificacaoRps']['Numero'] = str(Invoice)
	def Bill(self, Taker, Service):
		IdInvoice = self.Historic.get('Invoice')
		Invoice = keepdict('nfse/data/invoices/' + str(IdInvoice) + '.xml')
		Invoice['Invoice'] = OrderedDict()
		Invoice['Invoice']['Invoice'] = OrderedDict()
		Invoice['Invoice']['Invoice']['IdInvoice'] = str(IdInvoice)
		Invoice['Invoice']['Invoice']['TransmissionStatus'] = 'Send'
		Invoice['Invoice']['Invoice']['NfseNumber'] = ''
		Invoice['Invoice']['Invoice']['VerificationCode'] = ''
		Invoice['Invoice']['Invoice']['TicketId'] = ''
		Invoice['Invoice']['Provider'] = self.Provider
		Invoice['Invoice']['Taker'] = Taker
		Invoice['Invoice']['Service'] = Service
		if Taker.get("SujeitoAtivo") == "PJ":
			self.Template = OrderedDict(keepdict('nfse/data/TemplatePJ.xml'))
			self.setValues("TakerPJ", Taker)
		elif Taker.get("SujeitoAtivo") == "PF":
			self.Template = OrderedDict(keepdict('nfse/data/TemplatePF.xml'))
			self.setValues("TakerPF", Taker)
		self.setValues("Provider", self.Provider)			
		self.setIdRPS()
		self.setValues('Service',Service)
		XmlSigned = self.Signing(self.Template, self.Template['GerarNfseEnvio']['Rps']['InfDeclaracaoPrestacaoServico']['@Id'])
		XmlSigned['GerarNfseEnvio']['Rps']['Signature'] = XmlSigned['GerarNfseEnvio']['Signature']
		del XmlSigned['GerarNfseEnvio']['Signature']		
		Invoice['Invoice'].update(XmlSigned)
		Invoice.save()
		self.Historic['Invoice'] = int(self.Historic['Invoice']) + 1
		self.Historic.save()
		return IdInvoice
	def Issue(self, IdInvoice):
		if not self.Status:
			return False
		Invoice = keepdict('nfse/data/invoices/' + str(IdInvoice) + '.xml')
		if not bool(Invoice):
			return False
		if bool(Invoice.get('Invoice').get('Invoice').get('NfseNumber')):
			return True
		GerarNfseEnvio = Invoice.get('Invoice').get('GerarNfseEnvio')
		XmlSend = xmltodict.unparse({"GerarNfseEnvio": GerarNfseEnvio})
		Response = self.ZeepClient.service.GerarNfse(XmlSend)
		Return = xmltodict.parse(Response)
		GerarNfseResposta = Return.get('GerarNfseResposta')
		ListaMensagemRetorno = GerarNfseResposta.get('ListaMensagemRetorno')
		if bool(ListaMensagemRetorno):
			self.Status = False
			MensagemRetorno = ListaMensagemRetorno['MensagemRetorno']
			if type(MensagemRetorno) == list:
				for i in range(len(MensagemRetorno)):
					self.ErrorMessage.append(MensagemRetorno[i]['Mensagem'])
			else:
				self.ErrorMessage.append(MensagemRetorno['Mensagem'])
			return False
		else:
			Invoice['Invoice']['Invoice']['NfseNumber'] = GerarNfseResposta['ListaNfse']['CompNfse']['Nfse']['InfNfse']['Numero']
			Invoice['Invoice']['Invoice']['VerificationCode'] = GerarNfseResposta['ListaNfse']['CompNfse']['Nfse']['InfNfse']['CodigoVerificacao']
			Invoice['Invoice']['Invoice']['TransmissionStatus'] = 'Transmitted'
			Invoice['Invoice'].update(Return)	
			Invoice.save()
			return True
	def Signing(self, XmlDict, idURI):
		Xml = xmltodict.unparse(XmlDict)
		CertFile = open(os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")), self.PathCrt), "rb")
		ContentCert = CertFile.read()
		CertFile.close()
		CertKey = open(os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")), self.PathKey), "rb")
		ContentKey = CertKey.read()
		CertKey.close()
		root = etree.fromstring(Xml.encode())
		Signer = XMLSigner(
			method=methods.enveloped,
			signature_algorithm="rsa-sha1",
			digest_algorithm='sha1',
			c14n_algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315'
		)
		ns = dict()
		ns[None] = namespaces['ds']
		Signer.namespaces = ns
		Signature = Signer.sign(
			data=root,
			key=ContentKey,
			cert=ContentCert,
			reference_uri='#' + idURI
		)
		SignatureString = etree.tostring(Signature)
		Return = xmltodict.parse(SignatureString)
		return Return